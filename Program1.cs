﻿using System;
using System.IO;
using System.Net.Http;

namespace ConsoleApplication1
{
    class Program
    {

        static void Main(string[] args)
        {
            UriBuilder builder = new UriBuilder("http://naver.com");
            builder.Query = "no=1";

            HttpClient client = new HttpClient();
            var result = client.GetAsync(builder.Uri).Result;

            using (StreamReader sr = new StreamReader(result.Content.ReadAsStreamAsync().Result))
            {
                Console.WriteLine(sr.ReadToEnd());

            }
        }
    }
}

